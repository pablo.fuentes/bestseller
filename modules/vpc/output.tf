output "aws_vpc_id" {
  value = module.vpc.vpc_id
}
output "private_subnets_id_list" {
  value = module.vpc.private_subnets
}
output "public_subnets_id_list" {
  value = module.vpc.public_subnets
}
output "aws_allow_http_sg_id" {
  value = aws_security_group.allow_http.id
}
output "aws_allow_private_net_trafic" {
  value = aws_security_group.allow_public_net_trafic.id
}
output "private_sg" {
  value = aws_security_group.allow_public_net_trafic.id
}