module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "bestseller-vpc"
  cidr = var.vpc_cidr

  azs             = var.aws_avaibility_zones
  private_subnets = var.aws_private_nets_cidrs
  public_subnets  = var.aws_public_net_cidrs

  enable_nat_gateway = true
  enable_vpn_gateway = false

  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}
