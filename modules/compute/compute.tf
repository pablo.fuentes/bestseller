data "template_file" "user_data" {
  template = "${file("${path.module}/templates/user_data.tpl")}"

}
resource "aws_iam_instance_profile" "test_profile" {
  name = "best_profile"
  role = var.ec2_s3_role
}
module "asg" {
  source  = "terraform-aws-modules/autoscaling/aws"
  version = "~> 3.0"

  name = "best-autoscalling-g"

  lc_name = "best-lc"

  image_id             = var.aws_asc_ami_id
  instance_type        = var.aws_asc_instance_type
  security_groups      = [var.aws_asc_security_groups]
  iam_instance_profile = aws_iam_instance_profile.test_profile.name
  root_block_device = [
    {
      volume_size = "10"
      volume_type = "gp2"
    },
  ]

  # Auto scaling group
  asg_name                  = "best-asg"
  vpc_zone_identifier       = var.aws_asc_subnet
  health_check_type         = "EC2"
  min_size                  = 1
  max_size                  = 3
  desired_capacity          = 1
  wait_for_capacity_timeout = 0
  target_group_arns         = [aws_lb_target_group.best-target-group.arn]
  user_data                 = data.template_file.user_data.rendered

  tags = [
    {
      key                 = "Environment"
      value               = "dev"
      propagate_at_launch = true
    },
    {
      key                 = "Project"
      value               = "bestselelr-awesome"
      propagate_at_launch = true
    },
  ]

  tags_as_map = {
    kind = "terraform"
    Nmae = "tf-bestseller"
  }
}

resource "aws_autoscaling_policy" "heavy_out" {
  name                   = "heavy-out-asg-policy"
  scaling_adjustment     = 1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 30
  autoscaling_group_name = module.asg.this_autoscaling_group_name
}

resource "aws_cloudwatch_metric_alarm" "high_cpu_use" {
  alarm_name          = "heavy-asg-cpu-usage"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 1
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = 60
  statistic           = "Average"
  threshold           = 80
  dimensions = {
    AutoScalingGroupName = module.asg.this_autoscaling_group_name
  }
  alarm_description = "This metric monitors high EC2 CPU utilization"
  alarm_actions     = [aws_autoscaling_policy.heavy_out.arn]
}

resource "aws_autoscaling_policy" "heavy_in" {
  name                   = "heavy-in-asg-policy"
  scaling_adjustment     = -1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 30
  autoscaling_group_name = module.asg.this_autoscaling_group_name
}

resource "aws_cloudwatch_metric_alarm" "heavy_asg_cpu_usage_is_very_low" {
  alarm_name          = "heavy-asg-cpu-usage-is-very-low"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = 1
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = 60
  statistic           = "Average"
  threshold           = 60
  dimensions = {
    AutoScalingGroupName = module.asg.this_autoscaling_group_name
  }
  alarm_description = "This metric monitors EC2 CPU utilization"
  alarm_actions     = [aws_autoscaling_policy.heavy_in.arn]
}
