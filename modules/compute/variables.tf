variable "aws_vpc_id" {}
variable "aws_asc_subnet" {}
variable "aws_asc_security_groups" {}
variable "aws_asc_instance_type" {}
variable "aws_asc_ami_id" {}
variable "aws_elb_subnet" {}
variable "aws_elb_security_groups" {}
variable "ec2_s3_role" {}


