resource "aws_lb" "bestseller_elb" {
  name               = "best-lb-tf"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [var.aws_elb_security_groups]
  subnets            = var.aws_elb_subnet

  tags = {
    Environment = "Bestseller"
  }
}

resource "aws_lb_listener" "apache_listener" {
  load_balancer_arn = aws_lb.bestseller_elb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.best-target-group.arn
  }
}

resource "aws_lb_target_group" "best-target-group" {
  name     = "tf-best-lb-tg"
  port     = 80
  protocol = "HTTP"
  vpc_id   = var.aws_vpc_id

  health_check {
    healthy_threshold   = 10
    unhealthy_threshold = 2
    interval            = 10
    timeout             = 3
  }
}
