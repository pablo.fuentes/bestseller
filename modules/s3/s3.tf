resource "aws_s3_bucket" "bestseller-bucket" {
  bucket_prefix = var.s3_prefix
  region        = "us-east-1"
  tags = {
    Name        = "Bestseller-awesome-bucket-pablo"
    Environment = "Dev"
  }
}