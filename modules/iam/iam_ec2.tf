data "template_file" "sts_policy" {
  template = "${file("${path.module}/templates/sts_policy.json.tpl")}"
}

data "template_file" "s3_policy" {
  template = "${file("${path.module}/templates/s3_role_policy.jon.tpl")}"
}

resource "aws_iam_role" "sts_ec2_role" {
  name = "test_role"

  assume_role_policy = data.template_file.sts_policy.rendered

  tags = {
    tag-key = "tag-value"
  }
}

resource "aws_iam_instance_profile" "ec2_iam_profile" {
  name = "sts_ec2_profile"
  role = aws_iam_role.sts_ec2_role.name
}

resource "aws_iam_role_policy" "s3_aceess_policy" {
  name   = "s3_access_role"
  role   = aws_iam_role.sts_ec2_role.id
  policy = data.template_file.s3_policy.rendered
}