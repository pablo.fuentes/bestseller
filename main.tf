terraform {
  required_version = ">= 0.12.0"
  backend "s3" {
    bucket         = "bestseller-tf-state"
    key            = "tf-bestseller"
    region         = "us-east-1"
    dynamodb_table = "bestseller-tf-state"
  }
}

module "bestseller_s3" {
  source    = "./modules/s3"
  s3_prefix = "bestseller-bucket"
}

module "iam_ec2_s3" {
  source = "./modules/iam"
  s3_arn = module.bestseller_s3.aws_bestseller_s3_arn
}

module "bestseller_vpc" {
  source                 = "./modules/vpc"
  vpc_cidr               = "10.0.0.0/16"
  aws_avaibility_zones   = ["${var.aws_region}a", "${var.aws_region}b", "${var.aws_region}c"]
  aws_private_nets_cidrs = var.aws_private_nets_cidrs
  aws_public_net_cidrs   = var.aws_public_nets_cidrs
}

module "bestseller_compute" {
  source                  = "./modules/compute"
  aws_asc_instance_type   = var.aws_instance_type
  aws_asc_ami_id          = var.aws_ami_id
  aws_elb_subnet          = module.bestseller_vpc.public_subnets_id_list
  aws_elb_security_groups = module.bestseller_vpc.aws_allow_http_sg_id
  aws_asc_subnet          = module.bestseller_vpc.private_subnets_id_list
  aws_asc_security_groups = module.bestseller_vpc.aws_allow_private_net_trafic
  aws_vpc_id              = module.bestseller_vpc.aws_vpc_id
  ec2_s3_role             = module.iam_ec2_s3.iam_name
}
