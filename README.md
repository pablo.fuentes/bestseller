# Terraform for bestseller technical assignment
This repository contains a ready-to-build, infrastructure coded with [Terraform](https://www.terraform.io/)
## Prerequisites:

### Local development
#### Requirements:

- Terraform v0.12 (download [here](https://www.terraform.io/downloads.html))
- An AWS account with a user (AWS_ACCESS_KEY and AWS_ACCESS_SECRET) with admin permissions 
 
#### Setting up:

- Once you have Terraform downloaded, clone this repo
- login into aws [see here](https://docs.aws.amazon.com/es_es/cli/latest/userguide/cli-chap-configure.html)
- Note: in order to have Terraform working, make sure you have your backends created in your aws account, but don't worry, this is also automated
  - `cd remote_state/`
  - `terraform init` (with this you will initialize the remote state project)
  - `terraform apply`
  - Type `yes` when requested
  - Go back to the main directory
- Run `terraform init` to download all Terraform resources and modules 
- Run `terraform plan` so you can see what is Terraform going to create in the following step
- Run `terraform apply` and `yes` to start this process, it should not take more than 10 minutes
- Voila! there you have your awesome infra

## CI
In order to have a full automated process, this repo is automated with [Gitlab-Ci](https://docs.gitlab.com/ee/ci/) so you don  need even to turn on your computer. gitlab will do everything for you

#### Requirements:
- An AWS acoount with a user (AWS_ACCESS_KEY and AWS_ACCESS_SECRET) with admin permissions
- Go to Settings -> CI/CD ad click on "Variables" 
- Include your AWS access key and secret as values for the keys with the same name
- Go back to the CI/CD section of the menu and click the green buttom to create a new pipeline
- Once the plan stage success, you will have to trigger mannualy the "apply" stage, click in it and on the right side of the screen you should be able to see "Trigger manual action"
- Have another sip of coffee, in 5 minutes you will have all built 

## TODO
Yes, everything works, but we could do more to be awesome
- [ ] Add inputs as possible env vars, so we can edit them in the ci setting section instead of pushing a new commit
- [x] When pipeline applies, print the lb dns 
- [x] add linting stage to ci (check [here](https://github.com/hashicorp/terraform/issues/15304]))
- [ ] Create a new html file inside the instances to avoid seeing always the beautiful apahe one
- [ ] fix: name of aws region
- [ ] add certificate to lb
- [x] fix: name of aws region

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| aws\_ami\_id |  | string | `"ami-0b69ea66ff7391e80"` | no |
| aws\_instance\_type |  | string | `"t2.micro"` | no |
| aws\_private\_nets\_cidrs |  | list | `[ "10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24" ]` | no |
| aws\_public\_nets\_cidrs |  | list | `[ "10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24" ]` | no |
| aws\_region |  | string | `"us-east"` | no |


![yes we did it!](https://media.giphy.com/media/XbxZ41fWLeRECPsGIJ/giphy.gif)