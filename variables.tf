provider "aws" {
  region = "us-east-1"
}

variable "aws_region" {
  default = "us-east-1"
}

variable "aws_private_nets_cidrs" {
  default = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
}

variable "aws_public_nets_cidrs" {
  default = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]
}

variable "aws_ami_id" {
  default = "ami-0b69ea66ff7391e80"
}

variable "aws_instance_type" {
  default = "t2.micro"
}
